{{ with secret "pki_int/issue/cert" "role_name=keycloak" "common_name=keycloak.service.consul" "ttl=24h" "alt_names=_keycloak._tcp.service.consul,localhost" "ip_sans=127.0.0.1" }}
{{ .Data.private_key }}
{{ end }}
