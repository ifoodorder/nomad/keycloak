#!/bin/sh
openssl pkcs12 -export -out /opt/jboss/keycloak/standalone/configuration/application.keystore -inkey secrets/key.pem -in local/cert.pem -chain -CAfile local/cachain.pem -passin pass:password -passout pass:password -name server
chmod 644 /opt/jboss/keycloak/standalone/configuration/application.keystore
echo -e "\033[0;32mKeystore generated at /opt/jboss/keycloak/standalone/configuration/application.keystore\033[0m\n"
echo "Running Keycloak's entrypoint"
/opt/jboss/tools/docker-entrypoint.sh
