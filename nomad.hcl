job "keycloak" {
	datacenters = ["dc1"]
	type        = "service"
	group "keycloak" {
		network {
			port "http" {
				static = 8443
				to = 8443
			}
		}
		service {
			name = "keycloak"
			tags = ["auth"]
			port = "8443"
		}
		task "keycloak" {
			vault {
					policies = ["keycloak"]
			}
			resources {
				cpu    = 2000
				memory = 2000
			}
			template {
				data = file("cachain.pem.tpl")
				destination = "local/cachain.pem"
			}
			template {
				data = file("cert.pem.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("key.pem.tpl")
				destination = "secrets/key.pem"
			}
			template {
				data = file("file.env.tpl")
				destination = "secrets/file.env"
				env         = true
			}
			template {
				data = file("generate-keystore.sh")
				destination = "local/generate-keystore.sh"
				perms = "755"
			}
			driver = "docker"
			config {
				image = "quay.io/keycloak/keycloak:15.0.2"
				hostname = "keycloak"
				entrypoint = ["local/generate-keystore.sh"]
				ports = ["http"]
			}
		}
	}
}
