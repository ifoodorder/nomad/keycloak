DB_VENDOR = POSTGRES
DB_ADDR = postgresql.service.consul:5432
DB_DATABASE = keycloak
DB_PASSWORD = {{ with secret "database/creds/keycloak" }}{{ .Data.password }}
DB_USER = {{ .Data.username }}{{ end }}
KEYCLOAK_USER = admin
KEYCLOAK_PASSWORD = {{ with secret "secret/keycloak/admin" }}{{ .Data.password }}{{ end }}
KEYCLOAK_ENABLE_TLS = true
X509_CA_BUNDLE = /local/cachain.pem
